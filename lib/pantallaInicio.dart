//import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_sidem/formulario.dart';
//import 'formulario.dart';

// ignore: camel_case_types
class pantallaInicio extends StatefulWidget {
  @override
  _pantallaInicioState createState() => _pantallaInicioState();
}

// ignore: camel_case_types
class _pantallaInicioState extends State<pantallaInicio> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
           Image.asset("asset/images/logo.png"),
            RaisedButton (
              child: Text ("Registrar"),
              color: Colors.white38,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15),
                topRight: Radius.circular(15),
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15)
              )
              ),
              onPressed: () {
    Navigator.push(
    context,
    MaterialPageRoute(builder: (context)
    {
      return formulario();
    }),
    );

              },

            )
          ],
          ),
        ));
  }
}

