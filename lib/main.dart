import 'package:flutter/material.dart';
import 'package:flutter_sidem/pantallaInicio.dart';    
//función principal
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SIDEM',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: pantallaInicio(),
    );
  }
}


