//pantalla de los formularios

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'model/informacion.dart';
// ignore: camel_case_types
class formulario extends StatefulWidget {
  @override
  _formularioState createState() => _formularioState();
}

// ignore: camel_case_types
class _formularioState extends State<formulario> {
  final TextEditingController _controladorNombre = TextEditingController();
  final TextEditingController _controladorApellido= TextEditingController();
  final TextEditingController _controladorNumero = TextEditingController();
  bool val = false;
  onSwitchValueChanged (bool newVal){
    setState(() {
      val= newVal;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("SIDEM"),
          centerTitle: true,
    leading: new IconButton(
    icon: new Icon(Icons.room),
   ),
          actions: <Widget>[
          new IconButton(icon: new Icon(Icons.local_hospital) ),],
          automaticallyImplyLeading: false,
          backgroundColor: Colors.lightBlueAccent,),
      body:
      Padding (
        padding: const EdgeInsets.all((16.0)),
          child: Column (
            children: <Widget>[
              Padding (
                padding: const EdgeInsets.all(10.0),
                child: TextField (controller: _controladorNombre, decoration: InputDecoration(labelText: "Nombre"),
                keyboardType: TextInputType.text),
              ),
              Padding (
                padding: const EdgeInsets.all(10.0),
                child: TextField (controller: _controladorApellido, decoration: InputDecoration(labelText: "Apellido"),
                    keyboardType: TextInputType.text),
              ),
              Padding (
                padding: const EdgeInsets.all(10.0),
                child: TextField (controller: _controladorNumero, decoration: InputDecoration(labelText: "Numero de emergencia"),
                    keyboardType: TextInputType.text),
              ),
              Padding (
                padding: const EdgeInsets.only(left: 0.0, top: 90.0, right: 0.0, bottom: 0.0),
                child: Text("Ubicación",
                style: TextStyle (
                  fontSize: 15,
                  decoration: TextDecoration.underline,
                )),
              ),
              Switch (
                value: val,
                onChanged: (newVal){
                  onSwitchValueChanged (newVal);
                },

              ),


             Padding(
               padding: const EdgeInsets.only(left: 0.0, top: 160.0, right: 0.0, bottom: 0.0),
               child:

                   RaisedButton(

                     child: Text("Inicio"),
                     color: (Colors.lightBlueAccent),
                     elevation: 5.0,
                     shape: RoundedRectangleBorder(
                         borderRadius: BorderRadius.only(
                             topLeft: Radius.circular(15),
                             topRight: Radius.circular(15),
                             bottomLeft: Radius.circular(15),
                             bottomRight: Radius.circular(15)
                         )
                     ),

                     onPressed: (){
                       final String nombre= _controladorNombre.text;
                       final String apellido= _controladorApellido.text;
                       final int numero= int.tryParse (_controladorNumero.text);
                       final Informacion informacionNueva = Informacion (nombre, apellido, numero);
                       print (informacionNueva);
                     },
                   ),
             ),

            ],
          )
      ),
    );
  }
}